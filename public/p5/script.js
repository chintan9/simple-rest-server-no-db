var app = angular.module('productList', []);
var uid = 1;
app.factory('myfactory', ['$http', function($http) {
    return {
        getData: function(cb) {
            $http.get('/productlists').success(function(data) {
                cb(data)
            });
        }
    }
}]);
app.controller('productController', function($scope, myfactory, $http) {
    myfactory.getData(function(data) {
        $scope.products = data;
    });
    $scope.addproduct = function() {
        if ($scope.newproduct.uid == null) {
            $scope.newproduct.uid = uid++;
            $scope.products.push($scope.newproduct); // ajax
            var addproduct = $scope.newproduct;
        } else {
            for (i in $scope.products) {
                if ($scope.products[i].uid == $scope.newproduct.uid) {
                    $scope.products[i] = $scope.newproduct;
                }
            }
        }
        $scope.newproduct = {};
        console.log(addproduct);
        $http.post("/productlists", addproduct).success(function(addproduct) {})
    }
    $scope.delete = function(id) {
        var xdata = id;
        for (i in $scope.products) {
            if ($scope.products[i].id == id) {
                $http.delete("/productlists" + '/' + id).success(function(id) {
                    console.log(xdata + " is deleted");
                    $scope.products.splice(i, 1); // ajax
                })
                $scope.newproduct = {};
            }
        }
    }
    $scope.edit = function(id) {
        for (i in $scope.contacts) {
            if ($scope.contacts[i].id == id) {
                $scope.newcontact = angular.copy($scope.contacts[i]);
            }
        }
    }
});
