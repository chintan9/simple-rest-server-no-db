var $q = jQuery.noConflict();

function knowwhatbar(e, t) {
    if (t == 1) {
        if (e == "Keyword Search") {
            $q("#knintx").val("")
        }
    } else {
        if (e.length == 0) {
            $q("#knintx").val("Keyword Search")
        }
    }
}

function getoperation() {
    var e = false;
    var t = window.location.toString();
    t.match(/\?(.+)$/);
    var n = RegExp.$1;
    var n = n.split("&");
    var r = {};
    for (var i = 0; i < n.length; i++) {
        var s = n[i].split("=");
        r[s[0]] = unescape(s[1])
    }
    for (var o in r) {
        if (o == "operation") {
            if (r[o] == "eventdetail") {
                e = true
            }
        }
    }
    return e
}

function dropdown(e, t) {
    if (t == "1") {
        var n = {
            ocat: [{
                "All Entertainment": "Click>Cat_AllEntertain"
            }, {
                "Arts & Theatre": "Click>Cat_ArtsTheatre"
            }, {
                "Culinary & Dining": "Click>Cat_CulinaryDining"
            }, {
                "Family Fun": "Click>Cat_Family"
            }, {
                Fashion: "Click>Cat_Fashion"
            }, {
                Film: "Click>Cat_Film"
            }, {
                Music: "Click>Cat_Music"
            }, {
                Sports: "Click>Cat_Sports"
            }, {
                Other: "Click>Cat_Other"
            }]
        };
        var r = e.value;
        $q(n.ocat, function(e) {
            if (this.c) {
                omn_rmaction(momncommonur, this.c)
            }
        })
    } else {
        if (t == "2") {
            var i = {
                odma: [{
                    "Choose your location": "Click>DMA_ChooseLocation"
                }, {
                    "All Locations": "Click>DMA_AllLocation"
                }, {
                    Atlanta: "Click>DMA_Atlanta"
                }, {
                    Boston: "Click>DMA_Boston"
                }, {
                    Chicago: "Click>DMA_Chicago"
                }, {
                    "Florida Cities": "Click>DMA_Florida"
                }, {
                    Houston: "Click>DMA_Houston"
                }, {
                    "Los Angeles": "Click>DMA_LosAngeles"
                }, {
                    "New York": "Click>DMA_NewYork"
                }, {
                    Philadelphia: "Click>DMA_Philadelphia"
                }, {
                    "San Francisco": "Click>DMA_SanFrancisco"
                }, {
                    "Texas Cities": "Click>DMA_TexasCities"
                }, {
                    "Washington, DC": "Click>DMA_WashingtonDC"
                }, {
                    "More Cities": "Click>DMA_MoreCities"
                }]
            };
            var r = e.value;
            $q(i.odma, function(e) {
                if (this.c) {
                    omn_rmaction(momncommonur, this.c)
                }
            })
        } else {
            var s = window,
                r;
            r = e.value;
            var o = $q("#drop111");
            $q.each(o, function(e, t) {
                if (r == e.val()) {
                    omn_rmaction(momncommonur, "SelectOption>RecentlyViewed" + r);
                    s.location = e.id
                }
            })
        }
    }
}

function filterscript() {
    $q("#categorydrop").attr("readonly", true);
    $q("#locationdrop").attr("readonly", true);
    $q("#dijit_form_ComboBox_0").attr("readonly", true);
    $q("#filtercardlink").attr("target", "_blank");
    $q("#filtercardlink").attr("href", $q("#cardonloadurl").html().replace(/^\s+|\s+$/g, ""));
    $q("#allmonid").html("All Months");
    $q("#btopsearchbtn").click(function(e) {
        makesearch()
    });
    $q("BODY").click(function(e) {
        if (e.target.className == "cmaxtxtheadlink" || e.target.className == "float calendarclosei hover" || e.target.className == "float calendarclose hover" || e.target.className == "tcrtop" || e.target.className == "resnone" || e.target.id == "okbtn" || e.target.className == "enabdate" || e.target.className == "float filboxarrow" || e.target.className == "popuppcl" || e.target.className == "tedetxtcol1a float hover" || e.target.className == "float viewdetstyles sendinvi hover" || e.target.className == "emailicon" || e.target.className == "float emclosebtn" || e.target.className == "emsendembtn" || e.target.className == "closebtnthn" || e.target.className == "resnone" || e.target.className == "float resnone" || e.target.className == "evntterms1 hover" || e.target.className == "float viewdetstyles sendtofrn hover" || e.target.className == "valname emboxx" || e.target.className == "valemail emboxx" || e.target.id == "emcheck" || e.target.id == "textdate" || e.target.id == "textdate1") {} else {
            $q("#calendar").hide();
            if (getoperation()) {
                $q("#gocallayer, #emaillayer, #thankulayer, #tcpopup1a").hide()
            }
        }
        var t = $q("#calendar *");
        t.each(function(t) {
            if (e.target === this) {
                if (e.target.className == "float calendarclosei hover" || e.target.className == "float calendarclose hover" || e.target.id == "okbtn" || e.target.className == "enabdate" || e.target.id == "textdate" || e.target.id == "textdate1") {} else {
                    $q("#calendar").show()
                }
            }
        });
        var n = $q("#allmonthsid *");
        n.each(function(t) {
            if (e.target === this) {
                if (e.target.className == "float filboxarrow" || e.target.className == "popuppcl") {} else {
                    $q("#calendar").show()
                }
            }
        });
        $q(".calendarclose, .returntohome, .sendinvi, .sendtofrn, .printtxt, .evntterms1, #tedesccl1a1 .tedetxtcol1a, #tedesccl1a .tedetxtcol1a,.pointer, .login a,.emclosebtn").hover(function() {
            $q(this).css("text-decoration", "underline")
        }, function() {
            $q(this).css("text-decoration", "none")
        });
        if (getoperation()) {
            var r = $q("#gocallayer *");
            r.each(function(t) {
                $q(this).hover(function() {
                    $q(this).css("text-decoration", "underline")
                }, function() {
                    $q(this).css("text-decoration", "none")
                });
                if (e.target.className === "float viewdetstyles sendinvi hover" || e.target.id === "gcallink" || e.target.id === "ocallink" || e.target.parentNode.id === "ocallink" || e.target.className == "resnone") {
                    $q("#emaillayer, #tcpopup1a").hide();
                    $q("#gocallayer").show()
                } else {
                    $q("#gocallayer").hide()
                }
            });
            var i = $q("#emaillayer *");
            i.each(function(t) {
                if (e.target === this) {
                    if (e.target.id == "okbtn" || e.target.className == "enabdate" || e.target.className == "float filboxarrow" || e.target.className == "popuppcl" || e.target.className == "tedetxtcol1a float hover" || e.target.className == "float viewdetstyles sendinvi hover" || e.target.className == "emailicon" || e.target.className == "float emclosebtn" || e.target.className == "emsendembtn" || e.target.className == "closebtnthn" || e.target.className == "resnone" || e.target.className == "float resnone" || e.target.className == "evntterms1 hover" || e.target.className == "float viewdetstyles sendtofrn hover" || e.target.className == "valname emboxx" || e.target.className == "valemail emboxx" || e.target.id == "emcheck") {} else {
                        $q("#emaillayer").show()
                    }
                }
            });
            var s = $q("#thankulayer *");
            s.each(function(t) {
                if (e.target === this) {
                    if (e.target.id == "okbtn" || e.target.className == "enabdate" || e.target.className == "float filboxarrow" || e.target.className == "popuppcl" || e.target.className == "tedetxtcol1a float hover" || e.target.className == "float viewdetstyles sendinvi hover" || e.target.className == "emailicon" || e.target.className == "float emclosebtn" || e.target.className == "emsendembtn" || e.target.className == "closebtnthn" || e.target.className == "resnone" || e.target.className == "float resnone" || e.target.className == "evntterms1 hover" || e.target.className == "float viewdetstyles sendtofrn hover") {} else {
                        $q("#thankulayer").show()
                    }
                }
            });
            var o = $q("#tcpopup1a *");
            o.each(function(t) {
                if (e.target.className == "tedetxtcol1a float hover" || e.target.className == "float resnone") {
                    $q("#tcpopup1a").hide()
                }
            })
        }
    });
    loader()
}

function makenormal() {
    $q(".cardimgclass").each(function(e) {
        $q(this).animate({
            marginTop: "20"
        }, 200)
    })
}

function cardanimate(e, t) {
    makenormal();
    $q(e).animate({
        marginTop: "4"
    }, 200);
    $q("#filtercardlink").attr("href", t)
}

function cardopenlink(e) {
    window.open(e)
}

function momnall(e) {
    if (e == "1") {
        omn_rmaction(momncommonur, "DropDown>Cat")
    } else {
        if (e == "2") {
            omn_rmaction(momncommonur, "DropDown>DMA")
        } else {
            if (e == "3") {
                omn_rmaction(momncommonur, "DropDown>Calendar")
            } else {
                if (e == "4") {
                    omn_rmaction(momncommonur, "Button>Search")
                }
            }
        }
    }
}

function docalclick() {
    if ($q("#calendar").css("display") == "none") {
        $q("#calendar").show()
    } else {
        $q("#calendar").hide()
    }
    omn_rmaction("US:PersonalCards:ExploreRewardsBenefits:EntertainHome", "DropDown>Calendar")
}

function Recentclick() {
    omn_rmaction("US:PersonalCards:ExploreRewardsBenefits:EntertainHome", "DropDown>RecentlyViewed")
}

function makesearch() {
    var e = $q("#opt_c span").text();
    var t = e.split("&");
    if (t[1]) {
        e = t[0] + "And" + t[1]
    }
    var n = $q("#opt_l span").text();
    var r = $q("#knintx").val();
    var i;
    if (n === "All Locations" || n === undefined) {
        n = "Choose your location"
    }
    if (r == "Keyword Search") {
        if ($q("#allmonid").html() == "All Months") {
            i = "&category=" + e + "&location=" + n + "&template=templateA"
        } else {
            if ($q("#allmonid").html().split("-")[1] == undefined) {
                startdate = $q("#allmonid").html();
                i = "&category=" + e + "&location=" + n + "&startdate=" + startdate + "&template=templateA"
            } else {
                if ($q("#allmonid").html().split("-")[1]) {
                    startdate = $q("#textdate").val();
                    enddate = $q("#textdate1").val();
                    i = "&category=" + e + "&location=" + n + "&startdate=" + startdate + "&enddate=" + enddate + "&template=templateA"
                }
            }
        }
    } else {
        r = r.replace(/\'/g, "&#39;");
        r = r.replace(/\%/g, "&#37;");
        r = encodeURIComponent(r);
        if ($q("#allmonid").html() == "All Months") {
            i = "&category=" + e + "&location=" + n + "&template=templateA&textsearch=" + r + ""
        } else {
            if ($q("#allmonid").html().split("-")[1] == undefined) {
                startdate = $q("#allmonid").html();
                i = "&category=" + e + "&location=" + n + "&startdate=" + startdate + "&template=templateA&textsearch=" + r + ""
            } else {
                if ($q("#allmonid").html().split("-")[1]) {
                    startdate = $q("#textdate").val();
                    enddate = $q("#textdate1").val();
                    i = "&category=" + e + "&location=" + n + "&startdate=" + startdate + "&enddate=" + enddate + "&template=templateA&textsearch=" + r + ""
                }
            }
        }
    }
    window.location = "/entertainmentaccess/home.do?operation=search" + i
}

function EnterPressed(e) {
    var t;
    if (e && e.which) {
        e = e;
        t = e.which
    } else {
        t = e.keyCode
    }
    if (t == 13) {
        return true
    } else {
        return false
    }
}

function ifenter() {
    makesearch()
}

function loader() {
    $q("#year").html(monthrepalce.vals[b][b] + " " + a);
    clearweek();
    disdate(b, a);
    hideunwantedarrow()
}

function dec() {
    b--;
    if (b < 0) {
        b = 11;
        a = a - 1;
        $q("#year").html(monthrepalce.vals[b][b] + " " + a)
    } else {
        $q("#year").html(monthrepalce.vals[b][b] + " " + a)
    }
    clearweek();
    disdate(b, a);
    hideunwantedarrow();
    makedivwhite();
    omn_rmaction("US:PersonalCards:ExploreRewardsBenefits:EntertainHome", "Click>Calendar_LastMonth")
}

function inc() {
    b++;
    if (b > 11) {
        b = 0;
        a = a + 1;
        $q("#year").html(monthrepalce.vals[b][b] + " " + a)
    } else {
        $q("#year").html(monthrepalce.vals[b][b] + " " + a)
    }
    clearweek();
    disdate(b, a);
    hideunwantedarrow();
    makedivwhite();
    omn_rmaction("US:PersonalCards:ExploreRewardsBenefits:EntertainHome", "Click>Calendar_NextMonth")
}

function disdate(e, t) {
    d1.setYear(a);
    d1.setMonth(b, 1);
    var n = parseInt(d1.getDay());
    if (e == 0 || e == 2 || e == 4 || e == 6 || e === 7 || e == 9 || e == 11) {
        emptyspan(n);
        var r = n;
        for (i = 1; i <= 31; i++) {
            if (r <= 6) {
                spanify(i, t, e, "week1");
                r = r + 1
            } else {
                if (r > 6 && r <= 13) {
                    spanify(i, t, e, "week2");
                    r = r + 1
                } else {
                    if (r > 13 && r <= 20) {
                        spanify(i, t, e, "week3");
                        r = r + 1
                    } else {
                        if (r > 20 && r <= 27) {
                            spanify(i, t, e, "week4");
                            r = r + 1
                        } else {
                            if (r > 27 && r <= 34) {
                                spanify(i, t, e, "week5");
                                r = r + 1
                            } else {
                                if (r > 34 && r <= 41) {
                                    spanify(i, t, e, "week6");
                                    r = r + 1
                                }
                            }
                        }
                    }
                }
            }
        }
    } else {
        if (e == 3 || e == 5 || e == 8 || e == 10) {
            emptyspan(n);
            var r = n;
            for (i = 1; i <= 30; i++) {
                if (r <= 6) {
                    spanify(i, t, e, "week1");
                    r = r + 1
                } else {
                    if (r > 6 && r <= 13) {
                        spanify(i, t, e, "week2");
                        r = r + 1
                    } else {
                        if (r > 13 && r <= 20) {
                            spanify(i, t, e, "week3");
                            r = r + 1
                        } else {
                            if (r > 20 && r <= 27) {
                                spanify(i, t, e, "week4");
                                r = r + 1
                            } else {
                                if (r > 27 && r <= 34) {
                                    spanify(i, t, e, "week5");
                                    r = r + 1
                                } else {
                                    if (r > 34 && r <= 41) {
                                        spanify(i, t, e, "week6");
                                        r = r + 1
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } else {
            function s(e) {
                e = parseInt(e);
                if (e % 4 == 0) {
                    if (e % 100 != 0) {
                        return true
                    } else {
                        if (e % 400 == 0) {
                            return true
                        } else {
                            return false
                        }
                    }
                }
                return false
            }
            if (s(t)) {
                emptyspan(n);
                var r = n;
                for (i = 1; i <= 29; i++) {
                    if (r <= 6) {
                        spanify(i, t, e, "week1");
                        r = r + 1
                    } else {
                        if (r > 6 && r <= 13) {
                            spanify(i, t, e, "week2");
                            r = r + 1
                        } else {
                            if (r > 13 && r <= 20) {
                                spanify(i, t, e, "week3");
                                r = r + 1
                            } else {
                                if (r > 20 && r <= 27) {
                                    spanify(i, t, e, "week4");
                                    r = r + 1
                                } else {
                                    if (r > 27 && r <= 34) {
                                        spanify(i, t, e, "week5");
                                        r = r + 1
                                    } else {
                                        if (r > 34 && r <= 41) {
                                            spanify(i, t, e, "week6");
                                            r = r + 1
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                emptyspan(n);
                var r = n;
                for (i = 1; i <= 28; i++) {
                    if (r <= 6) {
                        spanify(i, t, e, "week1");
                        r = r + 1
                    } else {
                        if (r > 6 && r <= 13) {
                            spanify(i, t, e, "week2");
                            r = r + 1
                        } else {
                            if (r > 13 && r <= 20) {
                                spanify(i, t, e, "week3");
                                r = r + 1
                            } else {
                                if (r > 20 && r <= 27) {
                                    spanify(i, t, e, "week4");
                                    r = r + 1
                                } else {
                                    if (r > 27 && r <= 34) {
                                        spanify(i, t, e, "week5");
                                        r = r + 1
                                    } else {
                                        if (r > 34 && r <= 41) {
                                            spanify(i, t, e, "week6");
                                            r = r + 1
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

function spanify(e, t, n, r) {
    var i = new Date;
    i.setYear(t);
    i.setMonth(n, e);
    var s = new Date;
    var o = $q("<span/>").attr("id", n + 1 + "_" + t);
    o.html(e);
    if (i < s) {
        o.addClass("disabdate")
    } else {
        o.addClass("enabdate");
        if ($.browser.msie && $.browser.version <= 9) {
            o.click(function() {
                sidatedisp(this)
            })
        } else {
            o.attr("onclick", "sidatedisp(this)")
        }
    }
    o.appendTo($q("#" + r))
}

function emptyspan(e) {
    if (e > 0) {
        for (i = 1; i <= e; i++) {
            var t = $q("<span><span/>");
            $q("#week1").append(t)
        }
    }
}

function clearweek() {
    $q("#week1").html("");
    $q("#week2").html("");
    $q("#week3").html("");
    $q("#week4").html("");
    $q("#week5").html("");
    $q("#week6").html("")
}

function sidatedisp(e) {
    if (!textf && !textf1) {
        $q("#allmonid").html(e.id.split("_")[0] + "/" + $q(e).html() + "/" + e.id.split("_")[1]);
        $q("#calendar").hide()
    } else {
        if (textf) {
            $q("#textdate").val(e.id.split("_")[0] + "/" + $q(e).html() + "/" + e.id.split("_")[1]);
            omn_rmaction("US:PersonalCards:ExploreRewardsBenefits:EntertainHome", "Click>Calendar_RangeFirst")
        } else {
            if (textf1) {
                $q("#textdate1").val(e.id.split("_")[0] + "/" + $q(e).html() + "/" + e.id.split("_")[1]);
                omn_rmaction("US:PersonalCards:ExploreRewardsBenefits:EntertainHome", "Click>Calendar_RangeLast")
            }
        }
    }
}

function makedivwhite() {
    var e = $q("#wholecal").children("div");
    e.each(function(e) {
        $q(this).css("background-color", "white")
    })
}

function validate() {
    function n(e) {
        var t = false;
        if (e.split("/", 3)[2] && e.split("/", 3)[1] && e.split("/", 3)[0]) {
            if (isNaN(e.split("/", 3)[2]) || isNaN(e.split("/", 3)[1]) || isNaN(e.split("/", 3)[0])) {
                t = true
            }
        } else {
            t = true
        }
        return t
    }
    var e = $q("#textdate").val();
    var t = $q("#textdate1").val();
    if (e == "mm/dd/yyyy" && t == "mm/dd/yyyy") {
        $q("#calendar").hide();
        $q("#textdate").show();
        $q("#textdate1").show()
    } else {
        if (!n(e) && !n(t)) {
            var r = new Date;
            var i = new Date;
            r.setFullYear(e.split("/", 3)[2], e.split("/", 3)[0], e.split("/", 3)[1]);
            i.setFullYear(t.split("/", 3)[2], t.split("/", 3)[0], t.split("/", 3)[1]);
            if (i < r) {
                $q("#textdate1").attr("color", "red")
            } else {
                if (e != t) {
                    $q("#allmonid").html(e.split("/", 3)[0] + "/" + e.split("/", 3)[1] + "-" + t.split("/", 3)[0] + "/" + t.split("/", 3)[1])
                } else {
                    $q("#allmonid").html(e)
                }
                $q("#calendar").hide();
                $q("#textdate").show();
                $q("#textdate1").show()
            }
        } else {
            $q("#textdate").attr("color", "red");
            $q("#textdate1").attr("color", "red")
        }
    }
}

function clearcal() {
    $q("#allmonid").html("All Months");
    $q("#textdate").val("mm/dd/yyyy");
    $q("#textdate1").val("mm/dd/yyyy");
    $q("#textdate").attr("color", "black");
    $q("#textdate1").attr("color", "black")
}

function hideunwantedarrow() {
    $q("#weekpointerbar").css("margin-top", "-10px");
    var e = $q("#weekpointerbar input");
    e.each(function(e) {
        if ($q("#week" + (e + 1) + " .enabdate").length >= 1) {
            $q(this).show();
            $q(this).click(function(t) {
                makedivwhite();
                $q("#week" + (e + 1)).css("background-color", "lavender");
                $q("#textdate").val($q("#week" + (e + 1) + " .enabdate")[0].id.split("_")[0] + "/" + $q("#week" + (e + 1) + " .enabdate")[0].innerHTML + "/" + $q("#week" + (e + 1) + " .enabdate")[0].id.split("_")[1]);
                $q("#textdate1").val($q("#week" + (e + 1) + " .enabdate")[$q("#week" + (e + 1) + " .enabdate").length - 1].id.split("_")[0] + "/" + $q("#week" + (e + 1) + " .enabdate")[$q("#week" + (e + 1) + " .enabdate").length - 1].innerHTML + "/" + $q("#week" + (e + 1) + " .enabdate")[$q("#week" + (e + 1) + " .enabdate").length - 1].id.split("_")[1]);
                if ($q("#textdate").val() != $q("#textdate1").val()) {
                    $q("#allmonid").html($q("#week" + (e + 1) + " .enabdate")[0].id.split("_")[0] + "/" + $q("#week" + (e + 1) + " .enabdate")[0].innerHTML + "-" + $q("#week" + (e + 1) + " .enabdate")[$q("#week" + (e + 1) + " .enabdate").length - 1].id.split("_")[0] + "/" + $q("#week" + (e + 1) + " .enabdate")[$q("#week" + (e + 1) + " .enabdate").length - 1].innerHTML);
                    $q("#allmonid").html($q("#textdate").val())
                }
            })
        } else {
            $q(this).hide()
        }
    })
}
$q('.commonptrcls').each(function() {
    $q(this).hover(function() {
        $q(this).animate({
            marginLeft: "-5"
        }, 300)
    }, function() {
        $q(this).animate({
            marginLeft: "0"
        }, 300)
    })
});

function calclosee() {
    $q("#calendar").hide()
}
var momncommonur = "US:PersonalCards:ExploreRewardsBenefits:EntertainHome";
var monthrepalce = {
    vals: [{
        0: "Jan"
    }, {
        1: "Feb"
    }, {
        2: "Mar"
    }, {
        3: "Apr"
    }, {
        4: "May"
    }, {
        5: "Jun"
    }, {
        6: "Jul"
    }, {
        7: "Aug"
    }, {
        8: "Sep"
    }, {
        9: "Oct"
    }, {
        10: "Nov"
    }, {
        11: "Dec"
    }]
};
var d = new Date;
var d1 = new Date;
var a = d.getFullYear();
var b = d.getMonth();
var textf = false;
var textf1 = false;
