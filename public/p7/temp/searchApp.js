var params = {
    "q": "*", //Query String
    "x1": "country",
    "q1": "US",
    "sp_cs": "UTF-8",
    /*"x2": "eventcategory",
    "q2": "", // Category
    "x3": "eventmarket",
    "q3": "", //location */
    "sort": "eventsponsoredin|normalizedeventpresa|normalizedeventstart",
    "_": (new Date()).getTime()
};
angular.module('searchApp', ['ngSanitize']);
angular.module('searchApp').controller('searchCtrl', ['$scope', '$window', 'searchData', function($scope, $window, searchData) {
    $window.getEhubJSON = function(data) {
        $scope.data = data;
        console.log(data)
    };
    searchData.getEhubData();
    $scope.updatePage = function(link) {
        console.log(link);
        searchData.getEhubData(link);
    };
    $scope.buyTicket = function(link) {
        $window.open(link);
    };
    $scope.colors = {
        'Arts And Theatre': '#7b1470',
        'Music': '#14667b',
        'Sports': '#40a500',
        'Fashion': '#c84392'
    };
    $scope.check = function(link) {
        //console.log("You typed '" + $scope.searchQry + "'");
        if ($scope.searchQIn == undefined) {} else{params.q = $scope.searchQIn;};
        if ($scope.searchCat == undefined) {} else{params.q2 = $scope.searchCat;};
        if ($scope.searchLoc == undefined) {} else{params.q3 = $scope.searchLoc;};
        console.log(params);
        console.log(searchData.updateBaseLink(link));
        console.log(searchData.getEhubData(link));
    }
}]).factory('searchData', ['$http', function($http) {
    var baseDomain = 'https://sp100507ad.guided.ss-omtrdc.net/?';
    var getParams = function() {
        var str = '';
        for (var key in params) {
            str += key + '=' + encodeURIComponent(params[key]) + '&'
        };
        return str;
    };
    var baseLink = getParams();
    return {
        getEhubData: function(link) {
            link = link || baseLink;
            $http.jsonp(baseDomain + link);
        },
        updateBaseLink: function(link) {
            baseLink = link;
            return baseDomain + getParams();
        }
    };
}]);
